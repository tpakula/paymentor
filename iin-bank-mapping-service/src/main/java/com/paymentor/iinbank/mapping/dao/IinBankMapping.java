package com.paymentor.iinbank.mapping.dao;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Created by pakulat on 2016-07-20.
 */
@Entity
public class IinBankMapping {

    @Version
    @Column(name = "UPDATE_TS")
    private Calendar updateDate;


    @Column(name = "CREATION_TS", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Calendar creationDate;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public String getRange() {
        return range;
    }

    public String getBankId() {
        return bankId;
    }

    private String range;

    private String bankId;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    private boolean deleted = false;

    protected IinBankMapping() {
    }

    public IinBankMapping(String range, String bankId) {
        this.range = range;
        this.bankId = bankId;
    }

    @Override
    public String toString() {
        return range + ":" + bankId + ":" + deleted;
    }
}
