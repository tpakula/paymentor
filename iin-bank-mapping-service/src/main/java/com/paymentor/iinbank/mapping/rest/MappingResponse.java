package com.paymentor.iinbank.mapping.rest;

import java.util.Optional;

/**
 * Created by pakulat on 2016-07-21.
 */
public class MappingResponse {
    private final String cardNumber;

    private final String bankId;

    public MappingResponse(String cardNumber, Optional<String> bankId) {
        this.cardNumber = cardNumber;
        this.bankId = bankId.isPresent()?bankId.get():null;
    }

    public String getBankId() {
        return bankId;
    }

    public String getCardNumber() {
        return cardNumber;
    }


}
