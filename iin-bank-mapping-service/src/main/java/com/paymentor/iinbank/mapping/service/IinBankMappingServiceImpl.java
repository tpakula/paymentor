package com.paymentor.iinbank.mapping.service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paymentor.iinbank.mapping.service.exception.InvalidCardNumberException;
import com.paymentor.iinbank.mapping.service.exception.TechnicalException;

/**
 * Created by pakulat on 2016-07-20.
 */
@Service
public class IinBankMappingServiceImpl implements IinBankMappingService {

    private final MappingsProvider mappingsPorivder;

    private final RangeParser parser = new RangeParser(ServiceConfig.IIN_CHAR_LENGTH);


    @Autowired
    public IinBankMappingServiceImpl(MappingsProvider mappingsProvider) {
        this.mappingsPorivder = mappingsProvider;
    }

    public Optional<String> getBankId(String cardNumber) throws InvalidCardNumberException, TechnicalException {
        //TODO: validate input

        return findBankId(cardNumber);
    }

    public Map<String, Optional<String>> getBankIds(Collection<String> cardNumbers) throws InvalidCardNumberException, TechnicalException {
        //TODO; validate input
        Map<String, Optional<String>> result = cardNumbers.stream().collect(Collectors.toMap(e -> e, e -> findBankId(e)));
        return result;
    }

    private Optional<String> findBankId(String cardNumber){
        String iin = extractIIN(cardNumber);
        Integer iinInt = Integer.valueOf(iin);

        Optional<MappingEntry> result = mappingsPorivder.getAllMappings().stream()
                .filter(e -> e.getRange().contains(iinInt))
                .findFirst();

        if (result.isPresent()) {
            return Optional.<String>of(result.get().getBankId());
        } else {
            return Optional.empty();
        }
    }

    private String extractIIN(String cardNumber) {
        return cardNumber.substring(0, ServiceConfig.IIN_CHAR_LENGTH);
    }


}
