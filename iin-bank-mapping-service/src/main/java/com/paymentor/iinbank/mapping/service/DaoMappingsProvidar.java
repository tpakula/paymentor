package com.paymentor.iinbank.mapping.service;

import static com.paymentor.iinbank.mapping.service.ServiceConfig.IIN_CHAR_LENGTH;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Range;
import com.paymentor.iinbank.mapping.dao.IinBankMapping;
import com.paymentor.iinbank.mapping.dao.IinBankMappingRepository;

/**
 * Created by pakulat on 2016-07-21.
 */
@Component
public class DaoMappingsProvidar implements MappingsProvider {
    RangeParser rangeParser = new RangeParser(IIN_CHAR_LENGTH);

    private final IinBankMappingRepository repository;

    @Autowired
    DaoMappingsProvidar (IinBankMappingRepository repository){
        this.repository = repository;
    }

    @Override
    public List<MappingEntry> getAllMappings() {
        List<IinBankMapping> allActive = repository.findByDeleted(false);
        return allActive.stream().map(entry -> transformToEntry(entry)).collect(Collectors.toList());
    }

    private MappingEntry transformToEntry(IinBankMapping mapping){
        Range<Integer> range = rangeParser.fromString(mapping.getRange());
        return new MappingEntry(range, mapping.getBankId());
    }
}
