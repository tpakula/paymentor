package com.paymentor.iinbank.mapping.service;

import com.google.common.collect.Range;

/**
 * Created by pakulat on 2016-07-21.
 */
class MappingEntry {

    public Range getRange() {
        return range;
    }

    public String getBankId() {
        return bankId;
    }

    private final Range range;

    private final String bankId;

    MappingEntry(Range range, String bankId) {
        this.range = range;
        this.bankId = bankId;
    }
}
