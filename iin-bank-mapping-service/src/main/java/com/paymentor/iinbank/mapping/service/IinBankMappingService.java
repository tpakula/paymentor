package com.paymentor.iinbank.mapping.service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import com.paymentor.iinbank.mapping.service.exception.InvalidCardNumberException;
import com.paymentor.iinbank.mapping.service.exception.TechnicalException;

/**
 * Created by pakulat on 2016-07-20.
 *
 * Service to lookup BankId based on the card number
 *
 */
public interface IinBankMappingService {

    public Optional<String> getBankId(String cardNumber) throws InvalidCardNumberException, TechnicalException;

    public Map<String, Optional<String>> getBankIds(Collection<String> cardNumbers) throws InvalidCardNumberException, TechnicalException;
}
