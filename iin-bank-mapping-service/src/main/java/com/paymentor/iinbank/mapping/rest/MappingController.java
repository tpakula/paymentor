package com.paymentor.iinbank.mapping.rest;

import com.google.common.base.Stopwatch;
import com.paymentor.iinbank.mapping.service.IinBankMappingService;
import com.paymentor.iinbank.mapping.service.exception.InvalidCardNumberException;
import com.paymentor.iinbank.mapping.service.exception.TechnicalException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by pakulat on 2016-07-21.
 */
@RestController
@RequestMapping("/iinMapping/")
public class MappingController {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MappingController.class);

    @Autowired
    IinBankMappingService service;

    @RequestMapping("/")
    public String index() {
        return "I'm ALIVE";
    }

    @RequestMapping(value = "/getBankId/{cardNumber}", method = RequestMethod.GET)
    public MappingResponse getBankId(@PathVariable(value = "cardNumber") String cardNumber) throws TechnicalException, InvalidCardNumberException {
        //TODO add input validation

        log.info("Requst getBankId/{}", cardNumber);
        return new MappingResponse(cardNumber, service.getBankId(cardNumber));
    }

    @RequestMapping(value = "/getBankIds/{cardNumbers}", method = RequestMethod.GET)
    public List<MappingResponse> getBankIds(@PathVariable(value = "cardNumbers") List<String> cardNumbers) throws TechnicalException, InvalidCardNumberException {
        //TODO add input validation
        log.info("Requst getBankIds/{}", cardNumbers);
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<MappingResponse> result = service.getBankIds(cardNumbers).entrySet().stream()
                .map(entity -> new MappingResponse(entity.getKey(), entity.getValue()))
                .collect(Collectors.toList());

        stopwatch.stop();
        log.info("Request getBankIds/{} processed in {}[ms]", cardNumbers, stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return result;
    }

    @ExceptionHandler
    void handleIllegalArgumentException(InvalidCardNumberException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

}
