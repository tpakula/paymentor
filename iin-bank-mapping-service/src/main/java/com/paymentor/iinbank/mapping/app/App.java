package com.paymentor.iinbank.mapping.app;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.paymentor.iinbank.mapping.dao.IinBankMapping;
import com.paymentor.iinbank.mapping.dao.IinBankMappingRepository;

/**
 * Created by pakulat on 2016-07-20.
 */
@SpringBootApplication (scanBasePackages = {"com.paymentor.iinbank.mapping"})
@EnableJpaRepositories(basePackages = {"com.paymentor.iinbank.mapping.dao"})
@EntityScan(basePackages = {"com.paymentor.iinbank.mapping.dao"})
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public CommandLineRunner bootStrap(IinBankMappingRepository repository) {
        return (args) -> {
            // setup ranges
            repository.save(new IinBankMapping("5358-54**", "RolBank"));
            repository.save(new IinBankMapping("52", "nBank"));
            repository.save(new IinBankMapping("60-61", "Kakao SA"));
            repository.save(new IinBankMapping("5300-5358", "OMG Bank Łódzki"));

            IinBankMapping one = repository.findOne(1L);
            one.setDeleted(true);
            repository.save(one);

            // fetch all customers
            log.info("Mappings found with findAll():");
            log.info("-------------------------------");
            for (IinBankMapping mapping : repository.findAll()) {
                log.info(mapping.toString());
            }


            List<IinBankMapping> byDeleted = repository.findByDeleted(true);
            log.info("{}", byDeleted);

            log.info("{}", repository.getMaxUpdateDate().getTimeInMillis());
            log.info("");
        };
    }

}
