package com.paymentor.iinbank.mapping.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;

/**
 * Created by pakulat on 2016-07-20.
 */
public class RangeParser {

    public static final String IIN_RANGE_PATTERN = "(\\d{1,NOD})((-)((?=[\\d\\*]{1,NOD})\\d{1,NOD}\\*{0,NOD}))?";
    private final int numberOfDigits;
    private final Pattern validationPattern;

    public RangeParser(int numberOfDigits) {
        this.numberOfDigits = numberOfDigits;
        String patternString = IIN_RANGE_PATTERN.replaceAll("NOD", ""+numberOfDigits);
        this.validationPattern = Pattern.compile(patternString);
    }

    public Range<Integer> fromString(String rangeString) {
        Matcher matcher = validationPattern.matcher(rangeString);
        Preconditions.checkArgument(matcher.matches(), "Invalid range string: " + rangeString);
        Range<Integer> result = null;
        String[] parts = rangeString.split("-");

        if (parts.length == 1) {
            int length = parts[0].length();
            Preconditions.checkArgument(length <= numberOfDigits);
            int rangeStart = Integer.valueOf(parts[0]);
            int rangeEnd = rangeStart + 1;
            int rangeStartFilled = fillToNumberOfDigits(rangeStart, length);
            int rangeEndFilled = fillToNumberOfDigits(rangeEnd, length);
            result = Range.closedOpen(rangeStartFilled, rangeEndFilled);
        } else if (parts.length == 2) {
            RangeBorder rangeStart = tryParseStart(parts[0]);
            RangeBorder rangeEnd = tryParseEnd(parts[1]);
            Preconditions.checkArgument(rangeStart.rangeBorder <= rangeEnd.rangeBorder);
            result = buildRange(rangeStart, rangeEnd);
        } else {
            throw new IllegalArgumentException(rangeString + "  is not a valid range definition");
        }

        return result;
    }

    private int fillToNumberOfDigits(int value, int currentLength) {
        int result = value;
        for (int i = 0; i < numberOfDigits - currentLength; i++) {
            result = result *10;
        }
        return result;
    }

    private Range<Integer> buildRange(RangeBorder start, RangeBorder end){
        if (end.isClosed){
            return Range.closed(start.rangeBorder, end.rangeBorder);
        }else{
            return Range.closedOpen(start.rangeBorder, end.rangeBorder);
        }
    }

    private RangeBorder tryParseStart (String intString){
        Preconditions.checkArgument(intString.length() <= numberOfDigits);
        int start = Integer.valueOf(intString);
        int startFilled = fillToNumberOfDigits(start, intString.length());
        return new RangeBorder(startFilled, true);
    }

    private RangeBorder tryParseEnd (String intString){
        Preconditions.checkArgument(intString.length() <= numberOfDigits);
        boolean closed = intString.contains("*");
        String correctIntString = intString.replaceAll("\\*", "9");
        int end = Integer.valueOf(correctIntString);
        int endFilled = fillToNumberOfDigits(end, intString.length());
        return new RangeBorder(endFilled, closed);
    }

    private class RangeBorder {
        final int rangeBorder;
        final boolean isClosed;

        private RangeBorder(int rangeBorder, boolean isClosed) {
            this.rangeBorder = rangeBorder;
            this.isClosed = isClosed;
        }
    }
}
