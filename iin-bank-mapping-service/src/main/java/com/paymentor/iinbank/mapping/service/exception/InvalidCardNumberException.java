package com.paymentor.iinbank.mapping.service.exception;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by pakulat on 2016-07-20.
 */
public class InvalidCardNumberException extends Exception {

    private final List<String> cardsNumbers;

    public InvalidCardNumberException(List<String> cardsNumbers) {
        this.cardsNumbers = cardsNumbers;
    }

    @Override
    public String getMessage() {
        return "Invalid Card Numbers: " + StringUtils.join(cardsNumbers, ",");
    }
}
