package com.paymentor.iinbank.mapping.dao;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by pakulat on 2016-07-20.
 */
public interface IinBankMappingRepository extends CrudRepository<IinBankMapping, Long> {

    @Query("select max(m.updateDate) from IinBankMapping m")
    Calendar getMaxUpdateDate();

    List<IinBankMapping> findByDeleted(boolean deleted);
}
