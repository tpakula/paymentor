package com.paymentor.iinbank.mapping.service;

import java.util.List;

/**
 * Created by pakulat on 2016-07-21.
 */
interface MappingsProvider {

    /**
     * Returns list of all entries
     * @return
     */
    List<MappingEntry> getAllMappings();
}
