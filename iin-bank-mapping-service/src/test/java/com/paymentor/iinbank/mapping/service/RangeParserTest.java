package com.paymentor.iinbank.mapping.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Range;

/**
 * Created by pakulat on 2016-07-20.
 */
public class RangeParserTest {

    private RangeParser rangeParser;

    @Before
    public void before(){
        this.rangeParser = new RangeParser(4);
    }

    @Test
    public void shouldParseSingleEntryRange (){
        //given

        //when
        Range<Integer> range = rangeParser.fromString("62");
        //then
        int withinRange = 6299;
        int outOfRange = 6300;
        assertTrue(range.contains(withinRange));
        assertFalse(range.contains(outOfRange));

    }

    @Test
    public void shouldParseClosedOpenRange() {
        //given

        //when
        Range<Integer> range = rangeParser.fromString("62-63");

        //then
        int withinRange = 6299;
        int outOfRange = 6300;
        assertTrue(range.contains(withinRange));
        assertFalse(range.contains(outOfRange));
    }

    @Test
    public void shouldParseClosedRange() {
        //given

        //when
        Range<Integer> range = rangeParser.fromString("6298-62**");

        //then
        int withinRange = 6298;
        int outOfRange = 6300;
        assertTrue(range.contains(withinRange));
        assertFalse(range.contains(outOfRange));
    }


}