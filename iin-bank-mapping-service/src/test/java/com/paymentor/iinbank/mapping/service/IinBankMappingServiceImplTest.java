package com.paymentor.iinbank.mapping.service;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.google.common.collect.Range;
import com.paymentor.iinbank.mapping.service.exception.InvalidCardNumberException;
import com.paymentor.iinbank.mapping.service.exception.TechnicalException;

/**
 * Created by pakulat on 2016-07-21.
 */
public class IinBankMappingServiceImplTest {

    private static final String BANK_NAME = "bankname";

    @Test
    public void shouldReturnBankNameWhenCardInRange() throws TechnicalException, InvalidCardNumberException {
        //given
        MappingEntry mappingEntry = new MappingEntry(Range.closedOpen(6800, 6900), BANK_NAME);
        MappingsProvider mockProvider = Mockito.mock(MappingsProvider.class);
        Mockito.when(mockProvider.getAllMappings()).thenReturn(Arrays.asList(mappingEntry));

        IinBankMappingServiceImpl service = new IinBankMappingServiceImpl(mockProvider);

        String cardNumber = "6800123412341234";

        //when
        Optional<String> bankId = service.getBankId(cardNumber);

        //then
        Assert.assertTrue(bankId.isPresent());
        Assert.assertEquals(BANK_NAME, bankId.get());
    }

    @Test
    public void shouldReturnEmptyOptionalWhenCardOutsideOfKnownRange() throws TechnicalException, InvalidCardNumberException {
        //given
        MappingEntry mappingEntry = new MappingEntry(Range.closedOpen(6800, 6900), BANK_NAME);
        MappingsProvider mockProvider = Mockito.mock(MappingsProvider.class);
        Mockito.when(mockProvider.getAllMappings()).thenReturn(Arrays.asList(mappingEntry));

        IinBankMappingServiceImpl service = new IinBankMappingServiceImpl(mockProvider);

        String cardNumber = "6900123412341234";

        //when
        Optional<String> bankId = service.getBankId(cardNumber);

        //then
        Assert.assertFalse(bankId.isPresent());
    }

    @Test
    public void shouldReturnOneBankAndOneEmptySerachResult() throws TechnicalException, InvalidCardNumberException {
        //given
        MappingEntry mappingEntry = new MappingEntry(Range.closedOpen(6800, 6900), BANK_NAME);
        MappingsProvider mockProvider = Mockito.mock(MappingsProvider.class);
        Mockito.when(mockProvider.getAllMappings()).thenReturn(Arrays.asList(mappingEntry));

        IinBankMappingServiceImpl service = new IinBankMappingServiceImpl(mockProvider);

        String cardNumberIn = "6800123412341234";
        String cardNumberOut = "6900123412341234";
        List<String> queryParams = Arrays.asList(cardNumberIn, cardNumberOut);

        //when
        Map<String, Optional<String>> result = service.getBankIds(queryParams);

        //then
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(BANK_NAME, result.get(cardNumberIn).get());
        Assert.assertFalse(result.get(cardNumberOut).isPresent());
    }
}