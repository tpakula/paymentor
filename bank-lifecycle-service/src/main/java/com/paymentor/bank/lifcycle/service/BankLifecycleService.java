package com.paymentor.bank.lifcycle.service;

import com.paymentor.bank.lifcycle.service.exception.NoSuchBankException;
import com.paymentor.bank.lifcycle.service.exception.TechnicalException;

/**
 * Created by pakulat on 2016-07-20.
 */
public interface BankLifecycleService {
    Boolean isBankSuspended(String bankId) throws NoSuchBankException, TechnicalException;

}
